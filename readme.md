# Hair-DER

(= - =)

Based on [libWetHair](https://github.com/nepluno/libWetHair)

**Needed labraries:**
* GLEW
* GLFW
* ~~glm~~
* tbb
* Eigen
* rapidxml (already contained in include dir)
* AntTweakBar
* libpng++

Modelify the include path in CMakeLists.txt, then 

``` bash
  mkdir build
  cd build
  cmake ..
  make
```
