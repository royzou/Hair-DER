#include <GL/glew.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>

GLuint LoadShaders(const char* vertex_file_path, const char* fragment_file_path);